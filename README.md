# Geospatial Foundations

## Table of Contents  
* [Overview](#Overview)  
* [Environment](#Environment) 
* [License](#License) 
* [Installing](#Installing) 
* [Geospatial Data Extraction Pipelines](#Geospatial Data Extraction Pipelines) 
* [Geospatial Foundation Dataset](#Geospatial Foundation Dataset) 
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)


<a name="Overview"></a>
## Overview

Leveraging the APIs from Google Earth Engine and OpenStreetMap, this project established a series of automated data extraction pipelines and constructed a geospatial foundation dataset with satellite imagery and corresponding labels. The dataset is free, global-covered and up-to-date, which significantly lowers the cost and time for satellite imagery data annotation, and thus would help train deep neural networks that are frequently utilized within climate change research.


<a name="Environment"></a>
## Environment

This project is operating in the [Google Colab environment](https://colab.research.google.com/) where it has comprehensive and up-to-date functions and client libraries aligned with the [Google Earth Engine](https://earthengine.google.com/) so that project users do not need to configure the project environment from scratch. 


<a name="License"></a>
## License

In order to retrieve any geospatial data from Google Earth Engine, users need to sign up [here](https://signup.earthengine.google.com/#!/) for getting a license. 


<a name="Installing"></a>
## Installing

Project users need to use the package manager pip to install the [geopandas](https://geopandas.org/en/stable/) pakage.
```
pip install geopandas osmnx geemap
```

<a name="Geospatial Data Extraction Pipelines"></a>
## Geospatial Data Extraction Pipelines

The project has established satellite imagery extraction pipelines and label extraction pipeline retrieving imagery data and label data from Google Earth Engine and OpenStreetMap, respectively.

### National Agricultural Imagery Program Image Extraction Pipeline

This pipeline is to retrieve images from [National Agricultural Imagery Program](https://developers.google.com/earth-engine/datasets/catalog/USDA_NAIP_DOQQ) given the specified geographic locations and the size of area of interest.

* [Click here to open the image extraction pipeline for National Agricultural Imagery Program](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/blob/main/Code/NAIP_Imagery_Extraction.ipynb)

### Sentinel-2 Image Extraction Pipeline

This pipeline is to retrieve images from [Sentinel-2](https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR) mission given the specified geographic locations and the size of area of interest.

* [Click here to open the image extraction pipeline for Sentinel-2](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/blob/main/Code/Sentinel_2_Imagery_Extraction.ipynb)

### OpenStreetMap Label Extraction Pipelines

The following two pipelines are using the same functions but with different inputs and parameters to retrieve labels from [OpenStreetMap](https://www.openstreetmap.org/#map=13/36.0061/-78.9485) corresponding to the above National Agricultural Imagery Program and Sentinel-2 images, respectively.

* [Click here to open the label extraction pipeline for National Agricultural Imagery Program](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/blob/main/Code/NAIP_Label_Extraction.ipynb)

* [Click here to open the label extraction pipeline for Sentinel-2](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/blob/main/Code/Sentinel_2_Label_Extraction.ipynb)


<a name="Geospatial Foundation Dataset"></a>
## Geospatial Foundation Dataset

Utilizing the geospatial data extraction pipelines, the project has constructed a geospatial foundation dataset consisting of 568 satellite images and their corresponding labels, respectively. This dataset would be used to train the deep neural newworks which would help identify geographic changes within climate change research.

* [Click here to open the imagery data for National Agricultural Imagery Program](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/tree/main/Data%20Results/NAIP_Imagery)

* [Click here to open the imagery data for Sentinel-2](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/tree/main/Data%20Results/Sen2_Imagery)

* [Click here to open the label data for National Agricultural Imagery Program](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/tree/main/Data%20Results/NAIP_Labels)

* [Click here to open the label data for Sentinel-2](https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations/-/tree/main/Data%20Results/Sen2_Labels)

<a name="Authors"></a>
## Authors

Zhenxing Xie<br/>
Email: zhenxing.xie@duke.edu

Project Link:<br/> 
https://gitlab.oit.duke.edu/mids-capstone-projects/2021-2022/geospatial-foundations


<a name="Acknowledgments"></a>
## Acknowledgments

* [Google Earth Engine Tutorials](https://www.youtube.com/playlist?list=PLAxJ4-o7ZoPccOFv1dCwvGI6TYnirRTg3)
* [Earth Engine Python API Colab Setup](https://colab.research.google.com/github/google/earthengine-api/blob/master/python/examples/ipynb/ee-api-colab-setup.ipynb#scrollTo=LAZiVi13zTE7)
* [GeoPandas](https://github.com/geopandas/geopandas)
* [geemap module](https://geemap.org/geemap/)
* [osm module](https://geemap.org/osm/)
* [Loading Data from OpenStreetMap with Python and the Overpass API](https://towardsdatascience.com/loading-data-from-openstreetmap-with-python-and-the-overpass-api-513882a27fd0)

